#include <iostream>
#include <string.h>

using namespace std;

class String
{
  public :
    String () : str(nullptr), length(0) {};

    String (char c) : length(1) {
      str = new char[1];
      str[0] = c;
      cout << "Construction par char" << endl;
    }
    String (char * st) : length (strlen(st)){
      str = new char[length];
      cout << "Longeur " << length << endl; 
      for (int i = 0; i < length; i++){
        str[i] = st[i];
      } 
    }
    char * getString(){ 
      char * tmp = new char[length];
      for(int i = 0; i < length; i++){
        tmp[i] = str[i];
        cout << tmp[i];
      }
      return tmp;
    }

    String &operator+(const String &s) {
      int lgh = length + s.length;
      char * tmp = new char[lgh];
      for(int i = 0; i < length; i++){
          tmp[i] = str[i];
      }
      for (long i = 0; i < s.length; i++){
          tmp[length + i] = s.str[i];
      }
      cout << "Voici le resultat de la concat " << *this->getString() << endl;
      String nvs(tmp);
      return nvs;
    }

    bool operator==(const String &s) {
      if(length != s.length) return false;
      else {
        for(int i = 0; i < length; i++) {
          if((int)str[i] == (int)s.str[i]) continue;
          else return false;
        }
        return true;
      }
      return true;
    }

    String & operator=(String &s){
      length = strlen(s.str);
      for(int i = 0; i < length; i++){
        str[i] = s.str[i];
      }
      return *this;
    } 


    int getLongueur(){
      return length;
    }

    char getIenme(int i){
      return str[i];
    }
    
    ~String(){
      cout << "Je passe ici" << endl;
      delete [] str;
    };

  private:
  char *str;
  int length;

};

int main () {
  char a = 'a';
  String st(a);
  char *c = new char[3];
  c[0] = 'a';
  c[1] = 'b';
  c[2] = 'c';
  String st2(c);

 // String st5 = ('abc');

  char *c2 = new char[2];
  c2[0] = 'g';
  c2[1] = 'r';
  String st3(c2);

  char *c3 = new char[2];
  c3[0] = 'g';
  c3[1] = 'r';
  String st4(c3);

  cout << st2.getString() << endl;
  st2 + st3; 
  bool egal = st4 == st3;
  cout << "Les chaines sont égales : " << egal << endl;

  cout << "Longueur de la chaine " << st2.getLongueur() << endl;

  cout << "Le 3 eme élement : " << st2.getIenme(2) << endl;

  cout << "Test operator ="<< endl;
  st2 = st3;
  st2.getString();

  delete [] c;
  delete [] c2;
  delete [] c3;

  return 0;
}