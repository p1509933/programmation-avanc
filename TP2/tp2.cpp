#include <iostream>

using namespace std;

class LaClasse
{
  public :
    //Construction, conversion, affectation et destruction
    LaClasse() : l(0)
    {
      l = new int();
      std::cout << "LaClasse::LaClasse()\n";
    }
    LaClasse(const LaClasse & lc) : l(new int(*lc.l))
    {
      std::cout << "LaClasse::LaClasse(const LaClasse & )\n";}
    // Constructeur spécifique
    LaClasse(int *i) : l(i) 
    {
      std::cout << "LaClasse::LaClasse(int)\n";
    }
    operator bool() const
    {std::cout << "LaClasse::operator bool() const\n"; return true;}
    ~LaClasse()
    {
      delete l;
      std::cout << "~LaClasse::LaClasse()\n";
    }
    const LaClasse & operator=(const LaClasse & c)
    {l=c.l; std::cout << "LaClasse::operator=(const LaClasse &)\n"; return *this;}
    //Autre fonction membre
    LaClasse F(LaClasse);
    // Déclaration fonction extérieure amie
    friend std::ostream & operator << (std::ostream & os, const LaClasse & lc);

    // Constructeur par déplacement
    LaClasse(LaClasse && v)
    {
      l = v.l;
      v.l = nullptr;
    }

    // Operatuer d'affectation par déplacement
    const LaClasse & operator=(LaClasse && v)
    {
      if (this != &v)
      {
        delete l;
        l = v.l;
        v.l = nullptr;
      }
      cout << "Operator of deplacement" << endl;
      return *this;
    }

  protected :
    int *l;
};

LaClasse F(LaClasse vv)
{
  std::cout << " in F \n";
  return new int();
}

LaClasse LaClasse::F(LaClasse v)
{
  std::cout << " in LaClasse::F \n";
  return ::F(v);
}

std::ostream & operator << (std::ostream & os, const LaClasse & lc)
{
  os << " in ostream << LaClasse "<< lc.l << std::endl;
  return os;
}

// Class dérivé

class LaClasseSpecialisee : public LaClasse
{
  public:
    LaClasseSpecialisee()
    {
      cout << "Je passe dans le construc de la classe spécialiser" << endl;
    };
    const LaClasseSpecialisee & operator=(const LaClasseSpecialisee & c)
    {
      l = c.l; 
      std::cout << "Je passe das operator de LaClasseSpecialise" << endl;; 
      return *this;
    }
    ~LaClasseSpecialisee()
    {
      cout << "Je passe dans le destruc de la classe spécialiser" << endl;
    };
};



// Testez et analysez la séquence d'appels aux fonctions membres 
// de LaClasse dans le programme suivant :


int main()
{
  LaClasse c1;
  LaClasse c2=LaClasse();
  LaClasse cc1(c1);
  LaClasse cc2=c1;
  LaClasse cc3=LaClasse(c1);
  int *val = new int();
  *val = 5;
  LaClasse cv1(val);
  LaClasse cv2=val;
  LaClasse cv3=LaClasse(val);
  LaClasse cv4=(LaClasse)val;
  std::cout << std::endl;
  c1=cc1;
  std::cout << std::endl;
  int *val2 = new int();
  *val2 = 8;
  c2=val2;
  std::cout << std::endl;
  if(cv1)
    {
      int *val3 = new int();
      *val3 = 10;
      cv1=F(val3);
      cv1=F(c1);
      cv1=c1.F(c2);
    }

  std::cout << "Tableaux \n";
  LaClasse tab[3];
  LaClasse *pc=new LaClasse(tab[0]);
  delete pc;

  std::cout << "Operator of deplacement" << std::endl;
  LaClasse mv,mv2;
  mv = std::move(mv2);
  

  cout << "---- CLASS SPECIALISER -----" << endl;
  LaClasseSpecialisee cs;
  //LaClasseSpecialisee css;
  //cs = css;

  std::cout << "Avant de sortir ... \n";
  return 0; 
}
 