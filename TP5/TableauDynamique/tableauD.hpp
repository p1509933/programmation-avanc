#include <iostream>
using namespace std;

template <typename T,int AGRANDISSEMENT>
class TableauDynamique;

template <typename T,int A>
class Iterator
{
public:
	Iterator(): tabI(nullptr), pos(0){}

	Iterator(TableauDynamique<T,A> * tab){
		pos = 0;
		tabI = tab;
	}
	Iterator(TableauDynamique<T,A> * tab,int taille){
		pos = taille;
		tabI = tab;
	}

	Iterator<T,A> & operator++(){
		pos ++;
		return *this;
	}

	bool operator !=(const Iterator<T,A> &i) const {
		return (pos == i.pos) ? false : true;
	}
	// bool operator ==(Iterator<T,A> i){

	// }
	T& operator *() {
		return tabI->operator[](pos);
	}

	// T* operator()() {

	// };
	~Iterator(){};
private:
	TableauDynamique<T,A> * tabI;
	int pos;
};

template <typename T,int AGRANDISSEMENT>
class TableauDynamique
{
public:
	TableauDynamique(): tb(nullptr), taille(0), capacite(0){};

	TableauDynamique(const T t) : taille(1), capacite(AGRANDISSEMENT) {
		tb = new T[AGRANDISSEMENT];
		tb[0] = t;
	}

	template <typename A,int I> // car on passe un template en paramètre
	TableauDynamique(const TableauDynamique<A,I> &tab){
		tb = new T[tab.capacite];
		for(int i = 0; i < tab.taille; i++){
			tb[i] = tab.tb[i];
			taille = tab.taille;
			capacite = tab.capacite;
		}
	}

	void ajoute(T element){
		if(taille == capacite){
			this->upTab();
			tb[taille] = element;
		} else {
			taille ++;
			tb[taille] = element;
		}
	}

	T& operator[](int i){
	 	return tb[i];
	}

	template <typename A,int I>
	TableauDynamique<A,I> & operator=(const TableauDynamique<A,I> &tab){
		tb = new T[tab.capacite];
		for(int i = 0; i < tab.taille; i++){
			tb[i] = tab.tb[i];
			taille = tab.taille;
			capacite = tab.capacite;
		}
		return *this;
	}

	Iterator<T,AGRANDISSEMENT> begin(){
		Iterator<T,AGRANDISSEMENT> it(this);
		std::cout<< "Je passe Dans le begin" << endl;
 		return it;
	}

	Iterator<T,AGRANDISSEMENT> end(){
		Iterator<T,AGRANDISSEMENT> it(this,taille);
		std::cout<< "Je passe Dans le end" << endl;
		return it;
	}

	~TableauDynamique(){
		delete [] tb;
		capacite = taille = 0;
	};

private:
	T * tb;
	int taille;
	int capacite;

	T & upTab(){
		std::cout << "Le tableau à besoin d'un agrandissement" << std::endl;
		T * tmp = new T[capacite+AGRANDISSEMENT];
		for(int i = 0; i < taille;i++){
			tmp[i] = tb[i];
		}
		tb = tmp;
		capacite = capacite + AGRANDISSEMENT;
		delete [] tmp;
	}
};