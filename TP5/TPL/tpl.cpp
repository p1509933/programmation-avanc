#include <iostream>
#include <string.h>
using namespace std;

template <typename T>
T mySwap(T &s1, T &s2){
	T tmp = s2;
	s2 = s1;
	s1 = tmp;
}

template <typename T>
T myMin(const T &s1, const T &s2){
	return (s1 < s2) ? s1 : s2;
}


template <int  i>
const char * myMin(const char (&c1)[i], const char (&c2)[i]){
	if(strlen(c1) < strlen(c2)) return c1;
	if(strlen(c2) < strlen(c1)) return c2;
	else {
		int lenght = strlen(c1);
		for(int j = 0; j < lenght; j++){
			if(c1[j] < c2[j]){
				break;
				return c1;
			}if(c1[j] > c2[j]){
				break;
				return c2;
			}
		}
		const char * egal = "egal";
		return egal;
	}
}

template <>
char const * myMin(const char * const &c1,const char * const &c2){
	if(strlen(c1) < strlen(c2)) return c1;
	if(strlen(c2) < strlen(c1)) return c2;
	else {
		int lenght = strlen(c1);
		for(int i = 0; i < lenght; i++){
			if(c1[i] < c2[i]){
				break;
				return c1;
			}
			if(c2[i] < c1[i]){
				break;
				return c2;
			}
		}
		const char * egal = "egal";
		return egal;
	}	
}





int main(){
	std::cout << myMin(5,6) <<std::endl;
	std::cout << myMin(6,5) <<std::endl;
	std::cout << myMin("lili","lala") <<std::endl;
	//std::cout << myMin("li","lala") <<std::endl;
	const char * cc="mumu";
	const char * dd="ma";
	std::cout << myMin(cc,dd) <<std::endl;
	/*char ee[5]="toto";
	char ff[5]="ta"; //tableau de même taille que le précédent
	std::cout << min(ee,ff) <<std::endl;
	std::cout << min("zut",ff) <<std::endl;	*/
	return 0;
}