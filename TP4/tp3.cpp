#include <iostream>
using namespace std;

class Expression {
public:
	Expression();
	Expression(Expression & exp) = delete; // empêchement du constructeur par copie et par déplacement
  	Expression(Expression && exp) = delete;
	virtual ~Expression();
	virtual int eval() const = 0;
	virtual Expression * clone() const = 0;
};

class Constante : public Expression
{
public:
	Constante();
	Constante(char c) : i(c){};
	Constante(Constante & cons) = delete; // empêchement du constructeur par copie et par déplacement
  	Constante(Constante && cons) = delete;
	~Constante();
	int eval() const{
		return i;
	};
	Expression * clone() const{
		Constante *cons = new Constante(i);
  		return cons;
  	}
private:
	char i;	
};

class Plus : public Expression
{
public:
	Plus();
	~Plus();
	Plus(Expression *ed, Expression *eg){
		exG = eg->clone();
		exD = ed->clone();
	}
	Plus(Plus & pl) = delete; // empêchement du constructeur par copie et par déplacement
  	Plus(Plus && pl) = delete;
	int eval() const{
		return (exG->eval() + exD->eval());
	};
	Plus * clone() const{
		Plus * tmp = new Plus(exG,exD);
		return tmp;
	};
private:
	Expression * exG;
	Expression * exD;
	
};

class Moins : public Expression
{
public:
	Moins();
	~Moins();
	Moins(Expression *ed, Expression *eg){
		exG = eg->clone();
		exD = ed->clone();
	}
	Moins(Moins & mn) = delete; // empêchement du constructeur par copie et par déplacement
  	Moins(Moins && mn) = delete;
	int eval() const{
		return (exG->eval() - exD->eval());
	};
	Moins * clone() const{
		Moins * tmp = new Moins(exG,exD);
		return tmp;
	};
private:
	Expression * exG;
	Expression * exD;
};

class Mult : public Expression 
{
public:
	Mult();
	~Mult();
	Mult(Expression *ed, Expression *eg){
		exG = eg->clone();
		exD = ed->clone();
	}
	Mult(Mult & mlt) = delete; // empêchement du constructeur par copie et par déplacement
  	Mult(Mult && mlt) = delete;
	int eval() const{
		return (exG->eval() * exD->eval());
	};
	Mult * clone() const{
		Mult * tmp = new Mult(exG,exD);
		return tmp;
	};
private:
	Expression * exG;
	Expression * exD;
};

int main(){
	int a=5;
	const Expression & e = Mult(Plus( Constante(a), Constante(-2)),
	Plus( Constante(1),
	Constante(3)) );
	std::cout << e.eval() << std::endl;
	return 0;
}
